function [BETA, S_fk, states_time, states_seq] = readtrainremas(scofile)
%READTRAINREMAS   Read trained score.
%   [BETA, SFK, STATES_TIME, STATES_SEQ] = READTRAINREMAS(NAME)
%   reads parameters of a trained MIDI in ReMAS format.
%
% input:
%   NAME : name of the score file (ex.: Parameters.dat)
%
% output:
%   BETA        : beta
%   SFK         : 114 x N_BASES, spectral patterns for each combination
%   STATES_TIME : 2 x N_STATES, beginning and end frame of each state
%   STATES_SEQ  : 1 x N_STATES, combination index of each state
%

% Load score info
fid = fopen(scofile);
nbases  = fscanf(fid, '%d\n', 1);
nstates = fscanf(fid, '%d\n', 1);
BETA    = fscanf(fid, '%f\n', 1);
fclose(fid);

scopath = fileparts(scofile);
if isempty(scopath), scopath = '.'; end

% Read bases
fid = fopen([scopath '/Input/s_fk.bin'], 'r');
S_fk = fread(fid, [114 nbases], 'double');
fclose(fid);

if BETA~=0 && BETA~=1,
    S_fk = S_fk.^(1/(BETA-1));
end

% Read states_time
fid = fopen([scopath '/Input/states_time_e.bin'], 'r');
states_time_e = fread(fid, nstates, 'int64');
fclose(fid);
states_time(2,:) = states_time_e' + 1;

fid = fopen([scopath '/Input/states_time_i.bin'], 'r');
states_time_i = fread(fid, nstates,'int64');
fclose(fid);
states_time(1,:) = states_time_i' + 1;

% Read states_seq
fid = fopen([scopath '/Input/states_seq.bin'], 'r');
states_seq = fread(fid, nstates, 'int64');
fclose(fid);
states_seq = states_seq + 1;
