% createmidisynth.m
%
% Converts MIDI files (*.mid) in a folder to synthesized WAV.
%
% 2018
%

% Input and output folders
folder_mid = '/home/pcabanas/discosoundnas/Shizmidi/SCORE_DATA_SET/MIDI/';
folder_syn = '/home/pcabanas/discosoundnas/Shizmidi/SCORE_DATA_SET/SYNTH/';

% Create output folder, read input folder
mkdir(folder_syn);
D = dir([folder_mid '*.mid']);

for i = 1:numel(D),
    name    = D(i).name(1:end-4);
    midfile = [folder_mid D(i).name];
    synfile = [folder_syn name '.wav'];

    % Convert to wav
    fluidsynth(midfile, synfile);
end
