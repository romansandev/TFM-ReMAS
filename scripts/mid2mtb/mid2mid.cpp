/*=========================================================
 * mid2mid.cpp
 *
 * Rewrites MIDI file.
 *
 *   mid2mid(midIn, midOut)
 *
 * Based on the source code by Craig Stuart Sapp
 * (http://midifile.sapp.org).
 *
 * Written by Pablo Cabanas-Molero (2018).
 *
 *=======================================================*/

#include <iostream>
#include <math.h>
#include "mex.h"
#include "MidiFile.h"

using namespace std;
using namespace smf;

void _main();


/* The gateway routine */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    char      *filein, *fileout;
    int       status;
    MidiFile  midifile;

    /* Check number and type of arguments */
    if (nrhs != 2) {
        mexErrMsgIdAndTxt("MATLAB:mid2mid:nargin",
                "MID2MID requires two input arguments.");
    }
    if (nlhs != 0) {
        mexErrMsgIdAndTxt("MATLAB:mid2mid:nargout",
                "MID2MID does not have outputs.");
    }
    if (!mxIsChar(prhs[0]) || !mxIsChar(prhs[1])) {
        mexErrMsgIdAndTxt("MATLAB:mid2mid:invalidInputChar",
                "Arguments must be char.");
    }

    /* Get input data */
    filein  = mxArrayToString(prhs[0]);
    fileout = mxArrayToString(prhs[1]);

    /* Open MIDI file */
    status = midifile.read(filein);
    if (status == 0) {
        mexErrMsgIdAndTxt("MATLAB:mid2mid:corruptedMidi",
                "Could not read MIDI file.");
    }

	midifile.absoluteTicks();
    midifile.linkNotePairs();
    midifile.doTimeAnalysis();

    /* Write MIDI */
    midifile.write(fileout);

    return;
}
