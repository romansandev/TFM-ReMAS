/*=========================================================
 * mid2mtb.cpp
 *
 * Reads a MIDI file as a MIDI Toolbox compatible matrix.
 *
 * NMAT columns:
 *  1:  beat on time (in current bar)
 *  2:  beat duration
 *  3:  channel
 *  4:  pitch #
 *  5:  velocity
 *  6:  start time (seconds)
 *  7:  duration (seconds)
 *  8:  track
 *  9:  instrument
 *  10: tempo (bpm)
 *
 * Based on the source code by Craig Stuart Sapp
 * (http://midifile.sapp.org).
 *
 * Written by Pablo Cabanas-Molero (2018).
 *
 *=======================================================*/

#include <iostream>
#include <math.h>
#include "mex.h"
#include "MidiFile.h"

#define NCOLS 10

using namespace std;
using namespace smf;

void _main();


/* MIDI to matrix routine */
static void mid2mtb(MidiFile& midifile, double **nmat, int *nnotes)
{
    double tpq, tpb, qpb = 1, beat, tempo = 0;

    int    bpb = 4, otick = 0, chnn, *ischnn, *patch,
           nev, i, j = 0;

    /* Ticks per quarter note */
    tpq = (double) midifile.getTicksPerQuarterNote();
    tpb = tpq;

    /* Number of MIDI events */
    nev = midifile[0].size();

    /* Allocate memory for MIDI notes */
    *nmat = (double *) mxMalloc(NCOLS * nev * sizeof(double));

    /* Allocate memory for patches and channel flags */
    patch  = (int *) mxCalloc(16, sizeof(int));
    ischnn = (int *) mxCalloc(16, sizeof(int));

    for (i=0; i<nev; i++)
    {
        /* Key signature event */
        if (midifile[0][i].isTimeSignature())
        {
            // beats per bar (numerator)
            bpb = (int) midifile[0][i][3];
            // quarters per beat (4/denominator)
            qpb = 4 / pow(2, (double) midifile[0][i][4]);
            // ticks per beat
            tpb = tpq * qpb;

            // first tick of new signature
            otick = midifile[0][i].tick;
        }

        /* Tempo event */
        if (midifile[0][i].isTempo()) {
            tempo = midifile[0][i].getTempoBPM() / qpb;
        }

        /* Patch change event */
        if (midifile[0][i].isTimbre())
        {
            chnn = midifile[0][i].getChannelNibble();
            patch[chnn] = (int) midifile[0][i][1] + 1;
        }

        /* Note onset event */
        if (midifile[0][i].isNoteOn())
        {
            beat = (midifile[0][i].tick - otick) / tpb;
            chnn = midifile[0][i].getChannelNibble();

            ischnn[chnn] = 1;

            (*nmat)[j*NCOLS + 0] = fmod(beat, bpb) + 1;
            (*nmat)[j*NCOLS + 1] = midifile[0][i].getTickDuration() / tpb;
            (*nmat)[j*NCOLS + 2] = (double) chnn + 1;
            (*nmat)[j*NCOLS + 3] = (double) midifile[0][i][1];
            (*nmat)[j*NCOLS + 4] = (double) midifile[0][i][2];
            (*nmat)[j*NCOLS + 5] = midifile.getTimeInSeconds(0, i);
            (*nmat)[j*NCOLS + 6] = midifile[0][i].getDurationInSeconds();
            (*nmat)[j*NCOLS + 7] = (double) midifile[0][i].track + 1;
            (*nmat)[j*NCOLS + 8] = (double) patch[chnn];
            (*nmat)[j*NCOLS + 9] = (double) tempo;
            j = j + 1;
        }
    }

    /* Number of notes */
    *nnotes = j;

    /* Check channels without patches */
    for (i=0; i<16; i++) {
        if (ischnn[i] && !patch[i]) {
            mexWarnMsgIdAndTxt("MATLAB:mid2mtb:noPatches",
                    "Channel %d has no patches.", i+1);
        }
    }

    /* Check if no tempo */
    if (tempo == 0) {
        mexWarnMsgIdAndTxt("MATLAB:mid2mtb:noTempo",
                "MIDI does not have tempo messages.");
    }

    mxFree(patch);
    mxFree(ischnn);
    return;
}


/* The gateway routine */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    char      *filename;
    double    *nmat, *nmatout;
    int       nnotes, status, i, j;
    MidiFile  midifile;

    /* Check number and type of arguments */
    if (nrhs != 1) {
        mexErrMsgIdAndTxt("MATLAB:mid2mtb:nargin",
                "MID2MTB requires one input argument.");
    }
    if (nlhs != 1) {
        mexErrMsgIdAndTxt("MATLAB:mid2mtb:nargout",
                "MID2MTB requires one output argument.");
    }
    if (!mxIsChar(prhs[0])) {
        mexErrMsgIdAndTxt("MATLAB:mid2mtb:invalidInputType",
                "Input must be of type char.");
    }

    /* Open MIDI file */
    filename = mxArrayToString(prhs[0]);
    status   = midifile.read(filename);
    if (status == 0) {
        mexErrMsgIdAndTxt("MATLAB:mid2mtb:corruptedMidi",
                "Could not read MIDI file.");
    }

	midifile.absoluteTicks();
    midifile.linkNotePairs();
    midifile.joinTracks();
    midifile.doTimeAnalysis();

    /* Read MIDI */
    mid2mtb(midifile, &nmat, &nnotes);

    /* Create ouput matrix */
    plhs[0] = mxCreateDoubleMatrix(nnotes, NCOLS, mxREAL);
    nmatout = mxGetPr(plhs[0]);
    for (i=0; i<nnotes; i++) {
        for (j=0; j<NCOLS; j++) {
            nmatout[j*nnotes + i] = nmat[i*NCOLS + j]; //transpose
        }
    }

    mxFree(nmat);
    return;
}
