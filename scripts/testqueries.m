% testqueries.m
%
% Identifies audio excerpts (.wav) in "queries" folder.
%
% 2018
%

% Input folder
folder_qry = '/home/pcabanas/discosoundnas/Shizmidi/queries/20/';

% Result folder
folder_out = '/home/pcabanas/discosoundnas/Shizmidi/results/20/';

% Synth folder
folder_syn = '/home/pcabanas/discosoundnas/Shizmidi/SCORE_DATA_SET/SYNTH/';

% Create output folder, read input folder
mkdir(folder_out);
D = dir([folder_qry '*.wav']);

for i = 1:numel(D),
    wavfile = [folder_qry D(i).name];
    wavname = D(i).name(1:end-4);
    wavid   = wavname(4:7);

    % Identify score
    [ids, vals, pths] = shizmidi(wavfile);

    % Segment and alignment path of the found score
    pth1 = pths{1};
    x1 = audioread([folder_syn ids{1} '.wav'], [pth1(1,2)*570+1 pth1(end,2)*570+5700]);
    x1 = x1(:,1);

    % Find rank of the correct score
    scoids = char(ids);
    idx = strcmp(cellstr(scoids(:,1:4)), wavid);

    % Although first 4 numbers are unique, scores can be duplicated.
    % Check the rest of the name. The rank is the first encountered.
    rank = find(strcmp(cellstr(scoids(:,5:end)), cellstr(scoids(idx,5:end))));
    rank = rank(1);

    % Segment and alignment path of the correct score
    ptht = [];
    xt = [];
    if rank > 1,
        ptht = pths{rank};
        xt = audioread([folder_syn ids{rank} '.wav'], [ptht(1,2)*570+1 ptht(end,2)*570+5700]);
        xt = xt(:,1);
    end

    % Save result
    save([folder_out wavname '.mat'], 'ids','vals','rank','pth1','x1','ptht','xt');
end
