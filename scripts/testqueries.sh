
#!/bin/bash

# -*- ENCODING: UTF-8 -*-
# execute as:
# nohup sh ./testqueries.sh > ./testqueries.log 2>&1&

matlab -nodisplay -nosplash -nodesktop -r "try; run('./testqueries.m'); catch; end; quit"
