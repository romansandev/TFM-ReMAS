% createmiditrain.m
%
% Trains MIDI files (*.mid) in a folder.
%
% 2018
%

% Input MIDI and SYNTH folders
folder_mid = '/home/pcabanas/discosoundnas/Shizmidi/SCORE_DATA_SET/MIDI/';
folder_syn = '/home/pcabanas/discosoundnas/Shizmidi/SCORE_DATA_SET/SYNTH/';

% Ouput training folder
folder_dat = '/home/pcabanas/discosoundnas/Shizmidi/SCORE_DATA_SET/DAT';

% Create output folder, read input folder
mkdir(folder_dat);
D = dir([folder_mid '*.mid']);

parfor i = 1:numel(D),
    [~,name] = fileparts(D(i).name);
    midfile  = [folder_mid name '.mid'];
    synfile  = [folder_syn name '.wav'];

    % Train MIDI
    trainmidiremas(midfile, folder_dat, synfile);
end
