function [val, pth] = audio2score(wavfile, scofile)
%AUDIO2SCORE    Audio to score alignment.
%   [VAL, PTH] = AUDIO2SCORE(WAVFILE, SCOFILE) aligns audio
%   WAVFILE to score SCOFILE. SCOFILE must be in ReMAS
%   format (ex.: Parameters.dat). VAL is the alignment
%   measure and PTH is the alignment path.
%

% Load score data
[BETA, S_fk, states_time, states_seq] = readtrainremas(scofile);
nbases = size(S_fk, 2);

% Library adding
addpath ./NMF_States_Toolbox;

draw = 1;

% NMF params
NMFparams.B        = BETA;      % Beta divergence (0 Itakura, 1 Kullback, 2 EUC)
NMFparams.m_max    = 20;        % Number of harmonics
NMFparams.k_max    = nbases;    % Number of bases

% Load audio data
[ry, fs] = audioread(wavfile);
ry = sum(ry, 2)';
if fs ~= 44100,
    [P,Q] = rat(44100/fs);
    ry = resample(ry, P, Q);
    fs = 44100;
end
ry = ry/max(abs(ry));

% EXTRACT AUDIO FEATURES
rparam = fftParams(ry, fs);
rX_ft = computeCfreq(ry, rparam, draw);
rX_ft = computeNorma_B(rX_ft, NMFparams.B);

% AUDIO and SCORE MATCHING
NMFparams = NMF_setParams(rX_ft', rparam, NMFparams);
NMF_inic  = [];
NMF_inic.S_fk = S_fk;

% Local costs
[~, ~, ~, distorsionXstate] = SC_factorization_states(NMFparams, NMF_inic, rX_ft, draw);

% DTW
[~, p, q, val] = DTWoffline(states_seq, states_time, distorsionXstate, draw);
pth(:,1) = q;
pth(:,2) = p;
