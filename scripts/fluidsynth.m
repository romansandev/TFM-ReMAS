function [y, fs] = fluidsynth(midifile, wavfile)
%FLUIDSYNTH     MIDI synthesizer.
%   FLUIDSYNTH(MIDIFILE, WAVFILE) converts input midi file
%   MIDIFILE into audio file WAVFILE.
%   
%   [Y,FS] = FLUIDSYNTH(MIDIFILE, WAVFILE) returns also the
%   audio signal.
%
%   [Y,FS] = FLUIDSYNTH(MIDIFILE) only returns the signal. 
%

fluidexec = 'fluidsynth';                       % Path to fluidsynth
soundfont = './soundfonts/sf2/FluidR3_GM.sf2';  % Bank of soundfonts

midifile  = rtw_alt_pathname(midifile);

if nargin < 2,
    if ~nargout,
        error('output wav file not specified.');
    end
    wavfile = [tempname('.') '.wav'];
else
    wavfile = rtw_alt_pathname(wavfile);
    if exist(wavfile, 'file'),
        delete(wavfile);
    end    
end

system([fluidexec ' -F ' wavfile ' ' soundfont ' ' midifile]);

if nargout,
    [y, fs] = audioread(wavfile);
    y = y(:,1);
    if nargin < 2,
        fprintf('Deleting file ''%s''..\n', wavfile);
        delete(wavfile);
    end
end
