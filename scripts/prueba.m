
hold on;
for ii = 1:size(states_time, 2),
    comb = states_seq(ii);
    u1 = states_time(1, ii) - 0.5;
    u2 = states_time(2, ii) + 0.5;
    if comb~=1,
        for note = seq_notes{comb}(1,:),
            v1 = note - 23 - 0.5;
            v2 = note - 23 + 0.5;
            patch('Vertices', [u1 v1; u2 v1; u2 v2; u1 v2], 'Faces', [1 2 3 4], ...
                  'EdgeColor', 'w', 'FaceColor', 'none', 'LineWidth', 2);
        end
    end
end

%hold on;
%for ii = 1:size(nmat, 1),
%    u1 = max(ceil((44100*nmat(ii,6) - 5700/2 + 570)/570),1) - 0.5;
%    u2 = max(ceil((44100*(nmat(ii,6)+nmat(ii,7)) - 5700/2 + 570)/570),1) + 0.5;
%    v1 = nmat(ii,4) - 23 - 0.5;
%    v2 = nmat(ii,4) - 23 + 0.5;
%    patch('Vertices', [u1 v1; u2 v1; u2 v2; u1 v2], 'Faces', [1 2 3 4], ...
%          'EdgeColor', 'r', 'FaceColor', 'none', 'LineWidth', 2);
%end


