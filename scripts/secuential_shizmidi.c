/*Funciona todo salvo arreglar el tema de reservar bien la memoria para pths (por numero de tramas) y luego después del bucle ordenar las cosas*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ctimer.h"

char **D;

struct IdxVal{
    int idx;
    double val;
};

int compare (const void * a, const void * b)
{
    return ( (*(struct IdxVal*)a).val - (*(struct IdxVal*)b).val );
}

void Get_name(char *output ,char *input) //it parses the path to the wavfile and gets only the wavfile name
{
    char *token = strtok(input, "/");

    while(token != NULL)
    {
        strcpy(output, token);
        token = strtok(NULL, "/");
    }

}

void DB_list(int *sizeD, char *folder_dat)
{
    FILE *sysCallOutputStream;
    char sysCallOutput[100],
         name[200];
    snprintf(name, sizeof(name), "/bin/ls %s | /usr/bin/wc -l", folder_dat);
    sysCallOutputStream = popen(name, "r");

    if(sysCallOutputStream ==NULL)
    {
        printf("ERROR WHILE COUNTING NUMBER OF FILES ON %s\n", folder_dat);
        return;
    }

    fgets(sysCallOutput, sizeof(sysCallOutput) - 1, sysCallOutputStream);
    char *ptr;
    *sizeD = strtol(sysCallOutput, &ptr, 10);
    D = malloc(*sizeD * sizeof(char *));

    pclose(sysCallOutputStream);

    snprintf(name, sizeof(name), "/bin/ls %s", folder_dat);
    sysCallOutputStream = popen(name, "r");

    if(sysCallOutputStream == NULL)
    {
        printf("ERROR WHILE DOING LISTING CONTENT IN %s\n", folder_dat);
        exit(0);
    }

    int i = 0;
    while (fgets(sysCallOutput, sizeof(sysCallOutput) - 1, sysCallOutputStream) != NULL)
    {
        
         
        D[i] = (char *) malloc(sizeof(char) * (strlen(sysCallOutput) + 1) );
        strcpy(D[i], sysCallOutput);
	i++;
        
    }
    pclose(sysCallOutputStream);
}


int main (int argc, char *argv[]) 
{
    if(argc != 2)
    {
        printf("Correct usage is: ./shizmidi <path_to_wavfile>\n");
        return 0;
    }
    char *wavfile = malloc(sizeof(char) * strlen(argv[1]));
    strcpy(wavfile, argv[1]);
    //argv[1] = wavfile
    FILE *fid = malloc(sizeof(FILE)),
         *pthsfp = malloc(sizeof(FILE));
    char 
        *cpuaudio = "../remas/bin/CPUAudio ../remas/bin/Parameters.dat",
        *cpuremas = "../remas/bin/CPUReMAS",
        //Score database
        *folder_dat = "../SCORE_DATA_SET/DAT/";
    //List of scores
    
    int numMidis,
        j = 0,
	aux;
    
    //D and numMidis are filled
    DB_list(&numMidis, folder_dat);
    //Output buffers
    char **ids = malloc(sizeof(char *) * numMidis);
    struct IdxVal *vals = malloc(numMidis * sizeof(struct IdxVal));
    int **pths = malloc(sizeof(int *) * numMidis); //1538 is the number of values in output.path
    char *wavname = (char *)malloc(sizeof(char) * 100);

    //Extract features from wav
    Get_name(wavname, wavfile);

    char *wavfeatfile = (char *)malloc(sizeof(char) * (strlen(wavname) + 2));
    strncpy(wavfeatfile, wavname, (strlen(wavname) - 4));
    strcat(wavfeatfile, ".cfreq");
    char name[200];
    snprintf(name, sizeof(name), "%s %s %s", cpuaudio, argv[1], wavfeatfile);

    system(name);
    printf("\nCPUAdio has been succesfully executed\n");
    //Matches audio with each MIDI in the database
    //pragma omp parallel for private(name, fid)
    //all this should be private memory, since it is different for each loop iteration

    char sconame[100],
         datfile[100],
         outfile[100];
    
    double t1, t2, ucpu, scpu;
    ctimer( &t1, &ucpu, &scpu );
    
    for(int i = 0; i < numMidis; i++)
    {
        pths[i] = calloc(1538, sizeof(int));
	    D[i][strlen(D[i]) - 1] = '\0';
        strncpy(sconame, D[i], sizeof(sconame));
        snprintf(datfile, sizeof(datfile), "%s%s/Parameters.dat", folder_dat, sconame);
        snprintf(outfile, sizeof(outfile), "%s-vs-%s", wavname, sconame);

        //calculate matching value using REMAS

        snprintf(name, sizeof(name), "%s %s %s %s", cpuremas, datfile, wavfeatfile, outfile);

        system(name);
        snprintf(name, sizeof(name), "%s.vmin", outfile);
        fid = fopen(name, "rb");

        //Store matching value and DTW path for this MIDI
        ids[i] = (char *)malloc(sizeof(char) * strlen(sconame));
        strcpy(ids[i], sconame);
        fread(&vals[i].val, sizeof(double), 1, fid); //we will have to check if this is possible or it just reads strings
        vals[i].idx = i;
        //pths{i} = load([outfile '.path']); {matlab code} outfile.path is a file that describes an array of 1538 positions (for the example I have run)
        snprintf(name, sizeof(name), "%s.path", outfile);
        pthsfp = fopen(name, "r");
        if(pthsfp != NULL)
        {
            while(!feof(pthsfp))
            {
                fscanf(pthsfp, "%d  %d", &aux, &pths[i][j]);
		        j++;
            }
        }
	    j = 0;
        fclose(fid);
        snprintf(name, sizeof(name), "rm %s.path", outfile);
        system(name);
        snprintf(name, sizeof(name), "rm %s.vmin", outfile);
        system(name);
    }
    ctimer( &t2, &ucpu, &scpu );
    printf("time is: %.3f", t2-t1);
    snprintf(name, sizeof(name), "rm %s", wavfeatfile);
    system(name);

    printf("\nWe got to the end\n");

    qsort(vals, numMidis, sizeof(struct IdxVal), compare);
    printf("Identified score: %s (matching = %.3f)\n", ids[vals[0].idx], vals[0].val);
    //sort MIDIs according to matching
    /*matlab code
    [vals, idx] = sort(vals);
    ids  = ids(idx);
    pths = pths(idx);*/

    //Display result
    //printf("Identified score: %s (matching = %.3f)\n", ids[0], vals[0]);

    return 0;
}
