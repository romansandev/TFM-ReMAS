#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ctimer.h"
#include <omp.h>

char **D;

char *cpuaudio = "../remas/bin/CPUAudio ../remas/bin/Parameters.dat",
     *cpuremas = "../remas/bin/CPUReMAS",
     //Score database
     *folder_dat = "../SCORE_DATA_SET/DAT/";

typedef int bool;
#define true 1
#define false 0

//Output buffers
char **ids;
struct IdxVal *vals;
int **pths;

struct IdxVal{
    int idx;
    double val;
};

int compare (const void * a, const void * b)
{
    return ( (*(struct IdxVal*)a).val -(*(struct IdxVal*)b).val );
}

void Get_name(char *output ,char *input) //it parses the path to the wavfile and gets only the wavfile name
{
    char *local = malloc((strlen(input) + 1) * sizeof(char));
    strncpy(local, input, strlen(input));
    snprintf(local, strlen(input)+1, "%s", input);
    
    char *token = strtok(local, "/");
    
    while(token != NULL)
    {
        strcpy(output, token);
        token = strtok(NULL, "/");
    }
    free(local);
    free(token);
}

//From: https://www.linuxquestions.org/questions/programming-9/replace-a-substring-with-another-string-in-c-170076/
char *replace_str(char *str, char *orig, char *rep)
{
  static char buffer[4096];
  char *p;

  if(!(p = strstr(str, orig)))  // Is 'orig' even in 'str'?
    return str;

  strncpy(buffer, str, p-str); // Copy characters from 'str' start to 'orig' st$
  buffer[p-str] = '\0';

  sprintf(buffer+(p-str), "%s%s", rep, p+strlen(orig));

  return buffer;
}

void DB_list(int *sizeD, char *folder_dat)
{
    FILE *sysCallOutputStream;
    char sysCallOutput[100],
         name[200];
    snprintf(name, sizeof(name), "/bin/ls %s | /usr/bin/wc -l", folder_dat);
    sysCallOutputStream = popen(name, "r");

    if(sysCallOutputStream ==NULL)
    {
        printf("ERROR WHILE COUNTING NUMBER OF FILES ON %s\n", folder_dat);
        return;
    }

    fgets(sysCallOutput, sizeof(sysCallOutput) - 1, sysCallOutputStream);
    char *ptr;
    *sizeD = strtol(sysCallOutput, &ptr, 10);
    D = malloc(*sizeD * sizeof(char *));
    pclose(sysCallOutputStream);

    snprintf(name, sizeof(name), "/bin/ls %s", folder_dat);
    sysCallOutputStream = popen(name, "r");

    if(sysCallOutputStream == NULL)
    {
        printf("ERROR WHILE DOING LISTING CONTENT IN %s\n", folder_dat);
        exit(0);
    }

    int i = 0;
    while (fgets(sysCallOutput, sizeof(sysCallOutput) - 1, sysCallOutputStream) != NULL)
    {
        
         
        D[i] = (char *) malloc(sizeof(char) * (strlen(sysCallOutput) + 1) );
        strcpy(D[i], sysCallOutput);
        D[i][strlen(D[i]) - 1] = '\0';
	i++;
        
    }
    pclose(sysCallOutputStream);
}

void reorder(char **arr, struct IdxVal *index, int n)
{
    char **temp = malloc(sizeof(char *) * n * 3); //por alguna razón solo me reserva memoria para 103 posiciones sin el * 3, por eso reservo más memoria de la que hace falta
    // arr[i] should be present at index[i] index
    for (int i=0; i<n; i++)
    {
        temp[i] = malloc(sizeof(char) * (strlen(arr[index[i].idx]) + 1) );
        strcpy(temp[i], arr[index[i].idx]);
        temp[i][strlen(temp[i]) - 1] = '\0';
    }
    // Copy temp[] to arr[]
    for (int i=0; i<n; i++)
    { 
	strncpy(arr[i], temp[i], 100);
    //    index[i].idx = i;
    }
    free(temp);
}

void recursive( char *wavfile, double topWavs, int recursivityDepth, int numMidis, char *wavname, char *wavfeatfile)
{

    printf("recursivityDepth = %d\n", recursivityDepth);

    if(recursivityDepth < 1)
    {
        return;
    }
    else
    {
        printf("wavfile = %s\n", wavfile);
        printf("wavname = %s\n", wavname);
        FILE *fid = malloc(sizeof(FILE)),
            *pthsfp = malloc(sizeof(FILE));

        int j = 0;
        int aux;

        char sconame[100],
	     datfile[100],
             outfile[100],
             name[200];
        //#pragma omp parallel for shared(D, pths, aux) private(sconame, datfile, outfile, name, pthsfp, fid) firstprivate(j)
        for(int i = 0; i < numMidis; i++)
        {
	    if(recursivityDepth == 1)
		printf("LINEA\n");
            pths[i] = calloc(1538, sizeof(int));
            snprintf(sconame, 100, "%s", D[i]);
            snprintf(datfile, sizeof(datfile), "%s%s/Parameters.dat", folder_dat, sconame);
            snprintf(outfile, sizeof(outfile), "%s-vs-%s", wavname, sconame);
            //calculate matching value using REMAS
            
            snprintf(name, sizeof(name), "%s %s %s %s", cpuremas, datfile, wavfeatfile, outfile);
            system(name);
            snprintf(name, sizeof(name), "%s.vmin", outfile);
            fid = fopen(name, "rb");

            //Store matching value and DTW path for this MIDI
            ids[i] = (char *)malloc(sizeof(sconame));
            strcpy(ids[i], sconame);
            fread(&vals[i].val, sizeof(double), 1, fid); //we will have to check if this is possible or it just reads strings
            vals[i].idx = i;
            //pths{i} = load([outfile '.path']); {matlab code} outfile.path is a file that describes an array of 1538 positions (for the example I have run)
            snprintf(name, sizeof(name), "%s.path", outfile);
            pthsfp = fopen(name, "r");
            if(pthsfp != NULL)
            {
                while(!feof(pthsfp))
                {
                    fscanf(pthsfp, "%d  %d", &aux, &pths[i][j]); //dice que no existe esto: vfscanf.c
                    j++;
                }
            }
            fclose(pthsfp);
            j = 0;
            fclose(fid);
            snprintf(name, sizeof(name), "rm %s.path", outfile);

            system(name);
            snprintf(name, sizeof(name), "rm %s.vmin", outfile);

            system(name);
        }
        snprintf(name, sizeof(name), "rm %s", wavfeatfile);
        system(name);
        qsort(vals, numMidis, sizeof(struct IdxVal), compare);
        //reordenar vector D
        reorder(D, vals, numMidis);
        
        char *seconds = malloc(sizeof(char) * 4);
        strncpy(seconds, wavfile+10, 3);
        seconds[3] = '\0';
        if(strcmp(seconds, "/05") == 0)
        {
            recursive(replace_str(wavfile, "/05", "/10"), topWavs, --recursivityDepth, (int)(topWavs * numMidis), wavname, wavfeatfile);
        }
        else if(strcmp(seconds, "/10") == 0)
        {
            recursive(replace_str(wavfile, "/10", "/15"), topWavs, --recursivityDepth, (int)(topWavs * numMidis), wavname, wavfeatfile);
        }
        else if(strcmp(seconds, "/15") == 0)
        {
            recursive(replace_str(wavfile, "/15", "/20"), topWavs, --recursivityDepth, (int)(topWavs * numMidis), wavname, wavfeatfile);
        }

        // switch(seconds){
        //     case "/05":
        //                 recursive(replace_str(wavfile, "/05", "/10"), topWavs, --recursivityDepth, (int)(topWavs * numMidis));
        //                 break;
        //     case "/10":
        //                 recursive(replace_str(wavfile, "/10", "/15"), topWavs, --recursivityDepth, (int)(topWavs * numMidis));
        //                 break;
        //     case "/15":
        //                 recursive(replace_str(wavfile, "/15", "/20"), topWavs, --recursivityDepth, (int)(topWavs * numMidis));
        //                 break;
        //     case "/20":
        //                 break;
        // }
    }

}


int main (int argc, char *argv[]) 
{
    if(argc != 4)
    {
        printf("Correct usage is: ./shizmidi <path_to_wavfile> <top_percentage_scoring_results_to_reuse_(from 0 to 1)> <max_recursivity_deep>\n");
        return 0;
    }
    char *ptr,
         *wavfile = malloc(sizeof(char) * strlen(argv[1]));
    strcpy(wavfile, argv[1]);
    double topWavs = atof(argv[2]);
    int recursivityDepth = strtol(argv[3], &ptr, 10);

    //List of scores
    
    int numMidis;
    
    //D and numMidis are filled
    DB_list(&numMidis, folder_dat);
    char *wavname = (char *)malloc(sizeof(char) * 100);
    //Extract features from wav
    Get_name(wavname, wavfile);

    char *wavfeatfile = malloc(sizeof(wavname) + 2 * sizeof(char));
    strncpy(wavfeatfile, wavname, (strlen(wavname) - 4));
    strcat(wavfeatfile, ".cfreq");
    char name[200];
    snprintf(name, sizeof(name), "%s %s %s", cpuaudio, argv[1], wavfeatfile);

    system(name);
    printf("\nCPUAdio has been succesfully executed\n");
    //Matches audio with each MIDI in the database
    //pragma omp parallel for private(name, fid)
    //all this should be private memory, since it is different for each loop iteration

    ids = malloc(sizeof(char *) * numMidis);
    vals = malloc(numMidis * sizeof(struct IdxVal));
    pths = malloc(sizeof(int *) * numMidis);
    
    double t1, t2, ucpu, scpu;
    ctimer( &t1, &ucpu, &scpu );
    recursive(wavfile, topWavs, recursivityDepth, numMidis, wavname, wavfeatfile);

    ctimer( &t2, &ucpu, &scpu );
    printf("parallel time is: %.3f\n", t2-t1);

    printf("\nWe got to the end\n");

    printf("Identified score: %s (matching = %.3f)\n", ids[vals[0].idx], vals[0].val);
    //sort MIDIs according to matching
    /*matlab code
    [vals, idx] = sort(vals);
    ids  = ids(idx);
    pths = pths(idx);*/

    //Display result
    //printf("Identified score: %s (matching = %.3f)\n", ids[0], vals[0]);

    return 0;
}

