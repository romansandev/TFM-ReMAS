function trainmidiremas(midi, ruta, midisynth)
%TRAINMIDIREMAS  Train MIDI score for ReMAS.
%   TRAINMIDIREMAS(MIDI) trains a MIDI score and saves
%   the result in ReMAS format.
%
%   The ReMAS format is a subfolder with the same name as
%   the input MIDI and the following structure:
%
%   (folder) <MIDI>
%              |
%              |---> (file)   Parameters.dat
%              |
%              |---> (folder) INPUT
%                               |
%                               |---> (file) s_fk.bin
%                               |
%                               |---> (file) states_seq.bin
%                               |
%                               |---> (file) states_time_e.bin
%                               |
%                               |---> (file) states_time_i.bin
%
%   The file Parameters.dat is an ASCII file containing
%   3 values: number of combinations, number of states
%   and beta.
%
%   TRAINMIDIREMAS(MIDI, RUTA) saves the output subfolder
%   in RUTA ('.' by default).
%
%   TRAINMIDIREMAS(MIDI, RUTA, MIDISYNTH) provides the
%   synthesized MIDI in MIDISYNTH (WAV file). By default, it
%   is synthesized inside.
%

% Initial parameters
NMFparams.NUM_MAX_ITER = 50;    % Number of NMF iterations
NMFparams.lambda       = 0;     % Sparsity via penalty term
NMFparams.B            = 1.5;   % Beta divergence (0 Itakura, 1 Kullback, 2 EUC)
NMFparams.m_max        = 20;    % Number of harmonics

% Libraries adding
addpath ./NMF_States_Toolbox;
addpath ./mid2mtb;

% Output path
if nargin < 2 || isempty(ruta),
    ruta = '.';
end

% Create and/or read synthesized MIDI
if nargin < 3,
    midisynth = [tempname('.') '.wav'];
    [sy,fs] = fluidsynth(midi, midisynth);
    sy = sy(:,1)';
    delete(midisynth);
else
    [sy,fs] = audioread(midisynth);
    sy = sy(:,1)';
end

% Check synthesized MIDI
pot = sum(sy.^2);
if pot == 0,
    error('Invalid synthesized file for %s.', midi);
end

% Set some parameters
sparam  = getParametrosMusica_Oviedo(sy, fs);
ntramas = sparam.n_tramas;
[muestrasmidi, miditobins] = computeCfreqSeqInic(sparam);

cfreq_amplitudes = zeros(ntramas, muestrasmidi);
if size(cfreq_amplitudes,2) < sparam.midi_max,
    cfreq_amplitudes = [zeros(ntramas,sparam.midi_min-1), cfreq_amplitudes];
end

NMFparams = NMF_setParams(cfreq_amplitudes, sparam, NMFparams);

% Load MIDI data and extract state sequence
nmat = mid2mtb(midi);
nmat = nmat(:,1:7); % for compatibility

table = getNMATdataChannel(nmat, sparam);
[~, states_seq, states_time, ~, k_max] = NMF_states_generateEchannel(NMFparams, table);
NMFparams.k_max = k_max;

% LEARN COMBINATIONS BASES (except the first one)
NMFparams_nota = NMFparams;
NMFparams_nota.k_max = 1;

S_fk = ones(NMFparams.f_max, NMFparams.k_max);

for combination = 2:NMFparams.k_max,
    % Active frames of the current combination
    tramas_nota = [];
    seq_nota = find(states_seq==combination);
    for k = 1:length(seq_nota),
        tramas_nota = [tramas_nota, states_time(1,seq_nota(k)):states_time(2,seq_nota(k))];
    end

    % Center the frames in time, erasing begin and end of frames
    tramas_nota = creaTramasBien(tramas_nota, sparam);
    NMFparams_nota.t_max = length(tramas_nota);

    % Computing cfreq for the current combination
    cfreq_amplitudes_nota = computeCfreqSeqFrames(sy, sparam, tramas_nota, muestrasmidi, miditobins);
    if size(cfreq_amplitudes_nota,2) < sparam.midi_max,
        cfreq_amplitudes_nota = [zeros(NMFparams_nota.t_max,sparam.midi_min-1), cfreq_amplitudes_nota];
    end

    % Learn basis vector by NMF decomposition
    [S_f1, ~] = AlternatingFactorization(cfreq_amplitudes_nota, sparam, NMFparams);
    S_fk(:, combination) = S_f1;    
end

% Normalize bases
escalas = sum(S_fk.^NMFparams.B, 1).^(1/NMFparams.B) + eps;
S_fk = S_fk./repmat(escalas, NMFparams.f_max, 1);

% Store result
[~, name, ~] = fileparts(midi);
writetrainremas([ruta '/' name], NMFparams.B, S_fk, states_time, states_seq);
