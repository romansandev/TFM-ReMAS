% evaluateresults.m
%
% Gets statistics from a folder of results (.mat).
%
% 2018
%

% Input folder of results
folder_res = '/home/pcabanas/discosoundnas/Shizmidi/results/20/';

% Read folder
D = dir([folder_res '*.mat']);
N = numel(D);

ranks = zeros(1,N);
vals1 = zeros(1,N);
valst = zeros(1,N);
diffs = zeros(1,N);
plens = zeros(1,N);

fprintf('\nINCORRECT DETECTIONS:\n');
for i = 1:N,
    wavid = D(i).name(4:7);

    % Load data
    load([folder_res D(i).name]);

    ranks(i) = rank;
    vals1(i) = vals(1);
    valst(i) = vals(rank);
    diffs(i) = vals(2)-vals(1);
    if rank == 1,
        plens(i) = length(pth1);
    else
        plens(i) = length(ptht);
    end

    % Diffs may be wrong because of duplicated MIDIs
    j = 2;
    while isequal(ids{1}(5:end),ids{j}(5:end)),
        j = j+1;
    end
    diffs(i) = vals(j)-vals(1);

    % Print fails
    if ranks(i) > 1,
        fprintf('  - (%3d)  %s%-10s (top %3d) detected as  %-14s (%8.3f;  diff = %7.3f)\n',...
            i, wavid, ids{rank}(5:end), rank, ids{1}, vals(1), diffs(i));
    end
end

% Get statistics
idx = (ranks==1);
Nc  = sum(idx);
Nf  = N-Nc;
Acc = 100*Nc/N;
fprintf('\nNumber of scores correctly detected: %d/%d (%f)\n', Nc, N, Acc);
fprintf('Number of scores failed: %d/%d (%f)\n', Nf, N, 100-Acc);

idx5 = (ranks<=5);
Nc5  = sum(idx5);
Nf5  = N-Nc5;
Acc5 = 100*Nc5/N;
fprintf('Number of correct scores in Top-5: %d/%d (%f)\n', Nc5, N, Acc5);
fprintf('Number of correct scores out of Top-5: %d/%d (%f)\n', Nf5, N, 100-Acc5);

figure;
histogram(vals1(idx), 20, 'Normalization', 'probability');
hold on;
histogram(vals1(~idx),20, 'FaceColor', 'r', 'Normalization', 'probability');
title('valmin histogram');
legend('Correct (Top-1)', 'Incorrect (Top-1)');

figure;
histogram(vals1(idx5), 20, 'Normalization', 'probability');
hold on;
histogram(vals1(~idx5),20, 'FaceColor', 'r', 'Normalization', 'probability');
title('valmin histogram');
legend('Correct (Top-5)', 'Incorrect (Top-5)');

figure;
histogram(diffs(idx), 20, 'Normalization', 'probability');
hold on;
histogram(diffs(~idx),20, 'FaceColor', 'r', 'Normalization', 'probability');
title('valmin difference between 1st and 2nd');
legend('Correct (Top-1)', 'Incorrect (Top-1)');

figure;
histogram(plens(idx), 20, 'Normalization', 'probability');
hold on;
histogram(plens(~idx),20, 'FaceColor', 'r', 'Normalization', 'probability');
title('Path length of the correct score');
legend('Detected', 'Not detected');
