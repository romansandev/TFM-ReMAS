function writetrainremas(name, BETA, S_fk, states_time, states_seq)
%WRITETRAINREMAS   Save training parameters for ReMAS.
%   WRITETRAINREMAS(NAME, BETA, SFK, STATES_TIME, STATES_SEQ)
%   save parameters of a trained MIDI in ReMAS format.
%
% input:
%   NAME        : name of ouput folder
%   BETA        : beta
%   SFK         : 114 x N_BASES, spectral patterns for each combination
%   STATES_TIME : 2 x N_STATES, beginning and end frame of each state
%   STATES_SEQ  : 1 x N_STATES, combination index of each state
%

[N_MIDI, N_BASES] = size(S_fk);
N_STATES = length(states_seq);

if N_MIDI ~= 114,
    error('S_fk must have 114 x N_BASES elements.');
end

% Create output directory
mkdir(name);
mkdir([name '/Input']);

% File Paremeters.dat
fileID = fopen([name '/Parameters.dat'], 'w');
fprintf(fileID, [int2str(N_BASES)  '\n'], 'char');
fprintf(fileID, [int2str(N_STATES) '\n'], 'char');
fprintf(fileID, [num2str(BETA)     '\n'], 'char');
fclose(fileID);

% Files in Input subfolder
if BETA~=0 && BETA~=1,
    S_fk = S_fk.^(BETA-1);
end
fileID = fopen([name '/Input/s_fk.bin'], 'w');
fwrite(fileID, S_fk, 'double');
fclose(fileID);

states_seq = states_seq-1;
fileID = fopen([name '/Input/states_seq.bin'], 'w');
fwrite(fileID, states_seq, 'int64');
fclose(fileID);

states_time_i = states_time(1,:)-1;
fileID = fopen([name '/Input/states_time_i.bin'], 'w');
fwrite(fileID, states_time_i, 'int64');
fclose(fileID);

states_time_e = states_time(2,:)-1;
fileID = fopen([name '/Input/states_time_e.bin'], 'w');
fwrite(fileID, states_time_e, 'int64');
fclose(fileID);
