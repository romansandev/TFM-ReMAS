function [cfreq_amplitudes,miditobins]=computeCfreqSeqFrames(x,param,tramas_nota,muestrasmidi,miditobins)
% [cfreq_amplitudes,miditobins]=computeCfreqSeqFrames(x,param,tramas_nota)
% Define the default parameters from the input signal
%
% Inputs:
% x  - signal
% param - input parameters 
% tramas_nota - active frames of the current combination
%
% Outputs:
% cfreq_amplitudes - amplitude "MIDI" spectrogram (mnotesXframes)
% miditobins - Relation MIDI note & MIDI bins (2Xmnotes)
%
% Julio Carabias y Francisco Rodriguez. Fall 2012

% Inicializaciones
fs = param.fs;
longitud_frec = param.longitud_frec;
% n_tramas = param.n_tramas;
numero_tramas_nota = length(tramas_nota);

% Inicializacion de variables de salida
cfreq_amplitudes=zeros(numero_tramas_nota,muestrasmidi);
cfreq_amplitudes = cfreq_amplitudes';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Calculo el spectrograma secuencialmente
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

windowsize = param.windowsize;
salto = param.salto;
nfft=2*longitud_frec;
win = hanning(windowsize,'periodic');  % Quitado SQRT a la ventana. FCO. 24/11/11
noverlap = windowsize-salto;

if ~any(any(imag(x)))    % x purely real
    if rem(nfft,2),    % nfft odd
        select = [1:(nfft+1)/2];
    else
        select = [1:nfft/2+1];
    end
else
    select = 1:nfft;
end

nx = length(x);
nwind = length(win);
if nx < nwind    % zero-pad x if it has length less than the win length
    x(end+1:nwind)=0;  nx=nwind;
end
x = x(:);     % make a column vector for ease later
win = win(:); % be consistent with data set

zp = zeros(nfft-nwind,1);
nwind2 = (nwind- mod(nwind,2))/2;
wzp = [win(nwind2+1:nwind);zp;win(1:nwind2)]; % !____!

%nframes = n_tramas;
nframes = fix((nx-noverlap)/(nwind-noverlap));
frame_index = 1 + (0:(nframes-1))*(nwind-noverlap);
if length(x)<(nwind+frame_index(nframes)-1)
    x(end+1:nwind+frame_index(nframes)-1) = 0;   % zero-pad x
end

% X = zeros(length(select),nframes);
%for t=1:nframes,
for t=1:numero_tramas_nota,
    
    xframe = x(frame_index(tramas_nota(t)):frame_index(tramas_nota(t))+nwind-1); % extract frame of input data
    xzp = [xframe(nwind2+1:nwind);zp;xframe(1:nwind2)];
    xw = wzp .* xzp;
    Xframe = fft(xw); % FFT
    Xframe=Xframe(select); 
    
    % Calculo del espectrograma en frecuencia midi
    for midi_index=1:muestrasmidi,
        
        kmin=miditobins(1,midi_index);
        kmax=miditobins(2,midi_index);
        
        if (kmax-kmin)==0,
            value = abs(Xframe(kmin));
            cfreq_amplitudes(midi_index,t) = abs(Xframe(kmin));
        elseif (kmax-kmin)>0,
            value = sqrt(sum(abs(Xframe(kmin:kmax)).^2));
            cfreq_amplitudes(midi_index,t) = sqrt(sum(abs(Xframe(kmin:kmax)).^2)); % / (kmax-kmin+1));
        end;
    end;
end;

cfreq_amplitudes = cfreq_amplitudes';

return;