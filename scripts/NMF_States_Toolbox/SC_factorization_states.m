function [Y_ft,A_kt,S_fk,distorsionXstate]=SC_factorization_states(NMFparams,NMF_inic,X_ft,draw)
% [NMFparams,X_ft,Y_ft,A_kt,S_fk,distorsionXstate]=SC_factorization_states(NMFparams,NMF_inic,X_ft,draw)
% Estimate the projection for each state basis over the whole spectrogram
%
% Inputs:
% NMFparams - NMF default params
% NMF_inic  - NMF default matrixes
% X_ft      - Magnitude spectrogram
% draw - Show progress bar
%
% Outputs:
% Y_ft      - Estimated Magnitude spectrogram
% A_kt      - Gains
% S_fk      - Basis functions
% distorsionXstate - Distortion matrix
%
% Julio Carabias / Francisco Rodriguez -- December 2011

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
B     = NMFparams.B;
f_max = NMFparams.f_max;
t_max = NMFparams.t_max;
k_max = NMFparams.k_max;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Inicialize S
if isfield(NMF_inic,'S_fk'),
    S_fk=NMF_inic.S_fk;
else
    S_fk=ones(f_max,k_max) + rand(f_max,k_max);
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if draw,
    h = waitbar(0,'Please wait...','Name','Computing SC over real signal...');
end;

% Inicialize S and A per state
S_fk_limpio = zeros(size(S_fk));
A_kt_limpio = zeros(k_max,t_max);
A_kt_total = zeros(k_max,t_max);

% Inicialize distortion matrix
distorsionXstate = ones(k_max,t_max)*realmax;


norm_S=sum(S_fk.^B,1)+eps;
for k=1:k_max,
    if draw
        waitbar(k/k_max,h,['Note ' num2str(k) ' of ' num2str(k_max)]);
    end;
    
    %% Update A
    A_kt_total(k,:) = sum( bsxfun(@times,X_ft,S_fk(:,k).^(B-1)) ) / norm_S(1,k);
    
    % Generate S and A per state
    A_kt_limpio(k,:)=A_kt_total(k,:);
    S_fk_limpio(:,k)=S_fk(:,k);
    
    % Generate new Y
    Y_ft_limpio = (S_fk_limpio(:,k) * A_kt_limpio(k,:));
    
    % Compute distortion
    dmatrix = computedistorsionmatrix(NMFparams.B,X_ft,Y_ft_limpio);
    distorsionXstate(k,:) = sum(dmatrix);
    
    % Save gains in A_k
    A_kt_total(k,:,:)=A_kt_limpio(k,:,:);
    
    % Re-initialize variables
    S_fk_limpio(:,:,:) = 0;
    A_kt_limpio(:,:,:) = 0;
    
end;

% Generate A from individual A_k
A_kt=A_kt_total;

%% Generate Y
Y_ft=zeros(f_max,t_max);
for k=1:k_max,
    Y_ft = Y_ft + (S_fk(:,k) * A_kt(k,:));
end;

if draw  
    close(h);
end;

return;   


function [dmatrix] = computedistorsionmatrix(B,X_ft,Y_ft)
% [dmatrix] = computedistorsionmatrix(B,X_ft,Y_ft)
% Julio Carabias / Francisco Rodriguez -- December 2011

X_ft=X_ft+eps;
Y_ft=Y_ft+eps;

switch B
    case 0
        dmatrix = X_ft./Y_ft - log( X_ft./Y_ft ) - 1;        
    case 1
        dmatrix = X_ft .* (log(X_ft) - log(Y_ft)) + (Y_ft-X_ft);
    otherwise
        dmatrix = 1/(B*(B-1)) * ( X_ft.^B + (B-1)*Y_ft.^B - B*X_ft.*Y_ft.^(B-1) );        
end;

dmatrix=abs(dmatrix);

return;

