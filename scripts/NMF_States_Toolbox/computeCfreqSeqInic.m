function [muestrasmidi,miditobins]=computeCfreqSeqInic(param)
% [muestrasmidi,miditobins]=computeCfreqSeqInic(param)
% Define the default parameters from the input signal
%
% Inputs:
% param - input parameters 
%
% Outputs:
% muestrasmidi - frequency samples
% miditobins - Relation MIDI note & MIDI bins (2Xmnotes)
%
% Julio Carabias y Francisco Rodriguez. Fall 2012

% Inicializaciones
fs = param.fs;
longitud_frec = param.longitud_frec;
n_tramas = param.n_tramas;
midi_inc = param.midi_inc;
midi_min = param.midi_min;
midi_max = param.midi_max;

% Genero escala MIDI
miditobinskmin = zeros(1,(midi_max-midi_min+1)*midi_inc);
miditobinskmax = zeros(1,(midi_max-midi_min+1)*midi_inc);

for nota_midi=midi_min:midi_max,

    for midi_interval = 0:midi_inc-1,
        
        step_midi = 1/midi_inc;

        fmin=((2^(((nota_midi+midi_interval*step_midi-step_midi/2)-69)/12))*440);
        kmin=ceil(fmin/fs*(2*longitud_frec)+1);
        kmin=min(kmin,longitud_frec+1);

        fmax=((2^(((nota_midi+midi_interval*step_midi+step_midi/2)-69)/12))*440);
        kmax=fix(fmax/fs*(2*longitud_frec)+1);
        kmax=min(kmax,longitud_frec);

        miditobinskmin((nota_midi-midi_min)*midi_inc+midi_interval+1) = kmin;
        miditobinskmax((nota_midi-midi_min)*midi_inc+midi_interval+1) = kmax;
        
    end;

end;

if midi_inc==1,
    %miditobinskmin(miditobinskmax<miditobinskmin)=miditobinskmin(miditobinskmax<miditobinskmin);
    miditobinskmax(miditobinskmax<miditobinskmin)=miditobinskmin(miditobinskmax<miditobinskmin);
else   
    indval = (miditobinskmax>=miditobinskmin);
    miditobinskmin = miditobinskmin(indval);
    miditobinskmax = miditobinskmax(indval);
    minkmin = min(miditobinskmin); % Lineal hasta el primer midi
    miditobinskmin = [1:minkmin-1 miditobinskmin];
    miditobinskmax = [1:minkmin-1 miditobinskmax];
end;

miditobinskmax(end) = longitud_frec+1;
muestrasmidi = length(miditobinskmin);
miditobins=[miditobinskmin;miditobinskmax];

return;