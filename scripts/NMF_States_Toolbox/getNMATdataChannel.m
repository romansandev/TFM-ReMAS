function [table, nmat]=getNMATdataChannel(nmat, param)

table = zeros(size(nmat,1),7);
nmat = nmat(:,1:7);

for n = 1:length(nmat)
    table(n,1) = 0; 
    table(n,2) = 0;
    table(n,3) = ceil(nmat(n,6)*44100/570); 
    table(n,4) = floor((nmat(n,6)+nmat(n,7))*44100/570);  
%     table(n,3) = ceil(nmat(n,6)*param.fs/param.salto); 
%     table(n,4) = floor((nmat(n,6)+nmat(n,7))*param.fs/param.salto);  
    table(n,5) = nmat(n,6)*1000;  
    table(n,6) = nmat(n,4); 
    table(n,7) = nmat(n,3);
end

% % Create the alignment table to be returned by the scofo algorithm.
% %
% % ----------------------------------------------------------------------------------------------------------------------------------
% % Table info
% %          |       1            |          2         |       3        |       4       |        5         |      6     |      7     |
% %          | Estim. note start  |  Sequence position |  Onset frame   |  Offset frame |  MIDI start (ms) |  MIDI note |   Channel  |
% % ----------------------------------------------------------------------------------------------------------------------------------
% %
% % Input arguments: 
% %   nmat  - nmat midi info
% %	param - spectral features params
% %
% % Output: 
% %   nmat  - nmat truncated to the number of frames indicated in params  
% %   table - table info storage
% %
% % Julio Carabias. Septiembre 2015. Modified October 2017
% 
% % Load the MIDI matrix NMAT from the MIDI file? 
% % nmat = readmidi(ruta_midi);
% % nmat = midi2nmat(ruta_midi);
% % nmat = readmidi_java(ruta_midi);
% %
% % -----------------------------------------------------------------------------------------------
% % nmat INFO 
% %  |     1     |      2       |      3      |     4     |      5       |     6    |       7     |
% %  |   ONSET   |   DURATION   |   CHANNEL   |   PITCH   |   VELOCITY   |   ONSET  |   DURATION  |
% % -----------------------------------------------------------------------------------------------
% %
% 
% % Output var init
% table = zeros(nnotes(nmat),6);
% 
% % Start the iterative process for each instance in the NMAT matrix
% p=1; ini=0;
% while p <= nnotes(nmat) && ini <= param.n_tramas   
%     nota = pitch(nmat(p,:));    % Current MIDI note
%     canal = channel(nmat(p,:)); % Channel Number (i.e. Instrument)
%     
%     % Onset time (is samples)
%     nota_on_samples = onset(nmat(p,:),'sec')/param.ts; 
%     % Offset time (in samples)
%     nota_dur_samples = (onset(nmat(p,:),'sec')+dur(nmat(p,:),'sec'))/param.ts;
%     
%     % Onset time (in frames)
%     % ini=ceil( (nota_on_samples-param.windowsize/2)/param.salto );
%     ini=ceil( nota_on_samples/param.salto );
%     % if ini<1, ini=1; end; % Check first onset is >0
%     if ini<=1, ini=2; end   % Check first onset is >1 (required for onset detection)
%     
%     % Offset time (in frames)
%     % fin=floor( (nota_dur_samples+param.windowsize/2)/param.salto );
%     if ini == 8085,
%         1+1;
%     end
%     fin=floor( nota_dur_samples/param.salto );
%     if fin<ini, fin=ini; end  % Check never below onset
%     if fin>param.n_tramas, fin=param.n_tramas; end  % Check below max number of frames
%     
%     % Save info in the table
%     table(p,3:7)=[ini , fin , onset(nmat(p,:),'sec')*1000 , nota, canal];
%     % Move to the next instance in nmat
%     p=p+1;
% end
% 
% % Truncate the output vars to the maximum audio frames. Careful with p-1
% % since the while loop ends with p+1 
% table = table(1:p-1,:);
% nmat=nmat(1:p-1,:); 

return;