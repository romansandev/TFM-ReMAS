function [param]=getParametrosMusica_Oviedo(x,fs)
% [param]=getParametrosMusica(x,fs)
% Define the default parameters from the input signal
%
% Inputs:
% x the sinal
% fs frequency sampling rate
%
% Outputs:
% param - structure with default params

% Definicion de los parametros
param.fs=fs;              % Frequencia de muestreo
param.ts=1/param.fs;      % Periodo
param.delta_max=9;        % Numero de harmonicos
param.npolos=1;           % Numero de polos
%param.t_salto=0.004;     % voz 0.01 musica 0.004 voz
%param.t_trama=0.064;     % voz 0.128 musica 0.016 voz % comprobar interferencias!!!!
%param.t_salto=0.01;       % Salto en segundos
%param.t_trama=0.128;      % Tama??o de trama en tramas
%param.t_trama=0.046;      % Tama??o de trama en tramas
param.longitud_frec=8192; 

% Calculo el salto en tramas
param.salto=570; %round(param.t_salto*fs/2)*2; % para potencia de 2
% Calculo el tama??o de la ventana
param.windowsize=5700; %round(param.t_trama*fs/2)*2; % para potencia de 2

% Actualizamos los valores de tiempo
param.t_salto=param.salto/fs;
param.t_trama=param.windowsize/fs;

% Calculo el numero de tramas
param.n_tramas=fix((length(x)-param.windowsize)/param.salto)+1;

% Configuracion del cfreq
param.midi_min=24;             
param.midi_max=ceil(12*log2((fs/2)/440)+69);
param.umbral_perceptual=.25;
param.numtonos_maximo=100;

% Numero de MIDI bins por cada nota
param.midi_inc=1;

return;