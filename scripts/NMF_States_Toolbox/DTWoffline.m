function [matrizTmxTr,p,q,valmin] = ...
    DTWoffline(states_seq, states_time, distorsionXstate, draw)
% function [matrizTmxTr,p_final,q_final] = DTWoffline(states_seq, states_time, distorsionXstate, C)
% DTWsequence Find the most-probable (DTW) path through the distorition matrix.
% [p_final,q_final] = DTWsequence(states_seq, distorsionXstate,S_tmk)
%
% Inputs:
% states_seq - states sequence
% states_time - states duration
% distorsionXstate - Distortion Matrix
% C - Steps Cost
% draw - Plot results?
%
% Outputs:
% matrizSxD - Cost Matrix
% p_final(t) the path index
% q_final(t) the path itself

%% Init parameters
nstates = length(states_seq);
r_ntramas = size(distorsionXstate,2);
m_ntramas = states_time(2,end);

%% Generate matrizTmxTr
matrizTmxTr = zeros(m_ntramas,r_ntramas);
for ss_iter=1:nstates,
    current_state = states_seq(ss_iter);
    cstate_t_ini =  states_time(1,ss_iter);
    cstate_t_fin =  states_time(2,ss_iter);
    cstate_miditramas = cstate_t_fin - cstate_t_ini + 1;
    matrizTmxTr(cstate_t_ini:cstate_t_fin,:)=repmat(distorsionXstate(current_state,:),cstate_miditramas,1);
end;

% matrizTmxTr = matrizTmxTr ./ repmat(sqrt(sum(matrizTmxTr.^2,1)),m_ntramas,1);
% matrizTmxTr(~isfinite(matrizTmxTr)) = 0;
% 
% alpha=abs(log(1-0.99)/0.4);
% matrizTmxTr = 1-exp(-alpha*abs(matrizTmxTr));

[p,q,valmin] = dpfast_var_offline_min(matrizTmxTr,0);

if draw,
    figure;imagesc(matrizTmxTr);axis xy;hold on;plot(q,p,'k','LineWidth',3);drawnow;
end;
return;
