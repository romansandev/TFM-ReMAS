function [tramas_nota_limpia] = creaTramasBien(tramas_nota,param)

% Inicializaciones
% Centro de la trama
saltos_centra_trama = 0; % Esta hecho antes round((param.windowsize/param.salto)/2);
% Margen proteccion a principio y fin de trama
saltos_inicio_fin = fix(round((param.windowsize/param.salto)/2)/2);

% Tramas salida
tramas_validas = ones(size(tramas_nota));
tramas_validas(1) = 0;
tramas_nota_limpia = tramas_nota + saltos_centra_trama;

% Bucle para eliminar tramas no validas
longitud_nota_act = 1;
for t = 2:length(tramas_nota),
    % Incrementa o no la longitud de la nota actual
    if (tramas_nota_limpia(t) == (tramas_nota_limpia(t-1)+1)),
        longitud_nota_act = longitud_nota_act + 1;
    else
        % Elimina final de nota
        if longitud_nota_act > saltos_inicio_fin,
            tramas_validas(t-saltos_inicio_fin:t-1) = 0;
        end;
        % Reinicia longitud de nota
        longitud_nota_act = 1;
    end;
    
    if (longitud_nota_act <= saltos_inicio_fin),
        % elimina inicio de nota
        tramas_validas(t) = 0;
    elseif (tramas_nota_limpia(t)) > param.n_tramas,
        % Mayor que numero de tramas
        tramas_validas(t) = 0;
    end;
    
end;

tramas_nota_limpia = tramas_nota_limpia(tramas_validas==1);

% Si se pasa de eliminar tramas lo dejo como estaba
if (length(tramas_nota_limpia) < 1),
    tramas_nota_limpia = tramas_nota;
end;

    
return;