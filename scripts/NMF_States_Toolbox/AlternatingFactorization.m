function [S_f1,A_1t]=AlternatingFactorization(cfreq_amplitudes_nota,param,NMFparams)

% Inicializaciones
X_ft = cfreq_amplitudes_nota(:,param.midi_min:end)';
t_max = size(X_ft,2);
f_max = size(X_ft,1);
% S_f1 = zeros(f_max,1);
A_1t = zeros(1,t_max);
B = NMFparams.B;
NUM_ITER = 5;
distorsion = zeros(1,NUM_ITER);


% Estimacion inicial de bases mediante la media de las frec normalizadas
% Normalizacion
switch B
    case 0
        ener_per_frame=ones(1,t_max); %(sum(X_ft.^B,1)+eps);
    otherwise
        ener_per_frame=(sum(X_ft.^B,1)+eps).^(1/B);
end;
X_ft_norm = X_ft./repmat(ener_per_frame,size(X_ft,1),1);
S_f1 = mean(X_ft_norm,2);

% Bucle alternating
for iter=1:NUM_ITER,

    % Estimacion de ganancias
    A_1t = sum(X_ft.*repmat(S_f1.^(B-1),1,t_max),1) / (sum(S_f1.^B)+eps);
    
    % Estimacion de bases
    S_f1 = sum(X_ft.*repmat(A_1t.^(B-1),f_max,1),2) / (sum(A_1t.^B)+eps);
    
    % Normalizacion de bases
    switch B
        case 0
            escala = 1; %(sum(S_f1.^B,1)+eps);
        otherwise
            escala = (sum(S_f1.^B,1)+eps).^(1/B);
    end;
    S_f1 = S_f1 / escala;
    A_1t = A_1t * escala;
    
    % Calculo de la distorsion
    Y_ft = S_f1 * A_1t;
    switch B
        case 0
            distorsion(iter) = sum(sum(abs( Y_ft./(X_ft+eps) - log( (Y_ft./(X_ft+eps)) + eps ) - 1 )));
        case 1
            distorsion(iter) = sum(sum(abs( X_ft.*(log(X_ft+eps)-log(Y_ft+eps))+(Y_ft-X_ft) )));
        otherwise
            distorsion(iter) = sum(sum(abs( 1/(B*(B-1)) * ( X_ft.^B + (B-1)*Y_ft.^B - B*X_ft.*Y_ft.^(B-1) ) )));
    end;

end;

% Devolver las bases normalizadas a norma 2
escala = sqrt(sum(S_f1.^2,1)+eps);
S_f1 = S_f1 / escala;
A_1t = A_1t * escala;

return;

% norm_S=sum(S_fk.^B,1)+eps;

% A_kt_total(k,:) = sum(X_ft.*repmat(S_fk(:,k).^(B-1),1,t_max)) / norm_S(1,k);


