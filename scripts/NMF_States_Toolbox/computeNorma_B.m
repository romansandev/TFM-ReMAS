function [A_ft]=computeNorma_B(A_ft,B)
% [A_ft]=computeNorma_B(A_ft,B)
% Compute the beta normalization from the A_ft matrix per frame
%
% AJMM -- February 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if B == 1
    norma = sum(A_ft);
    A_ft = bsxfun(@times, A_ft, 1./norma);
    
elseif B == 0
    norma = size(A_ft,1);
    A_ft = A_ft / norma;
    
else
    norma = sum(A_ft.^B).^(1/B);
    A_ft = bsxfun(@times, A_ft, 1./norma);
    
end
end