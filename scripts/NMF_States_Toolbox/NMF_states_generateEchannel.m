function [table,states_seq,states_time,states_notes,k_max] = NMF_states_generateEchannel(NMFparams,table)
% [E_kp,S_tmk,states_seq,states_seq_onsets,states_time,k_max]=NMF_states_generateEchannel(NMFparams,R_ptj,R_ptj_onsets)
% Generate the score-units (Combinations) and the sequence (States) from the MIDI transcription
%
% Inputs:
% NMFparams - NMF default params
% Table info
% ----------------------------------------------------------------------------------------------------------------------------------
%          |       1            |          2         |       3        |       4       |        5         |      6     |      7     |
%          | Estim. note start  |  Sequence position |  Onset frame   |  Offset frame |  MIDI start (ms) |  MIDI note |   Channel  |
% ----------------------------------------------------------------------------------------------------------------------------------
%
% Outputs:
% table             - Info
% states_seq        - States time sequence
% states_time       - States t_ini:t:fin (from MIDI transcription)
% states_notes      - Tuple with [Notes per state; Channel per note]
% k_max             - Number of combinations (score units)
%
% Julio Carabias. Fall 2017
%

% Load Parameters
t_max = NMFparams.t_max; % Number MIDI frames
n_max = size(table,1);   % Number note events in the score

%%%%%%%

% Current capacity (Optimization)
BLOCK_SIZE = 200;
seq_Size = BLOCK_SIZE;

% Output vars init 
states_seq = zeros(1,seq_Size);
states_time = zeros(2,seq_Size);
states_notes = cell(1,seq_Size);

% Fist state SILENCE init
states_seq(1)=1;       % State number
states_time(1,1)=1;    % t_ini
states_time(2,1)=1;    % t_fin
states_notes{1}=[0;0]; % Primer estado es silencio

% Pointers to last free position
comb_Ptr = 2;
seq_Ptr = 2;

% DEBUG PARAMETERS %
t_parada = 8085;
% ----------------

%% FUNCTION BODY
for t=2:t_max
    % DEBUG PARAMETERS %
        if t==t_parada
        end
    %     if seq_Ptr==8
    %     end
%     if seq_Ptr ==407,
%         1+1;
%     end
    % ------------------ %
    
    % Get current active notes at frame t
    note_counter=0;      % Silence control
    active_notes=[];     % Active notes at t
    active_channels=[];  % Active channels at t
    table_index=[];      % Table indexes
    for n=1:n_max
        if t>=table(n,3) && t<=table(n,4) 
            note_counter    = note_counter+1; 
            active_notes    = [active_notes table(n,6)];
            active_channels = [active_channels table(n,7)]; 
            table_index     = [table_index n];   
        end
    end
    
    %% CASE 1: NO NOTE ACTIVITY
    if note_counter==0 
        % IF SILENCE, increment duration of the silence state
        if states_seq(seq_Ptr-1) == 1 
            states_time(2,seq_Ptr-1) = t;
        else % ELSE, assign silence index (unit = 1) to the sequence 
            states_seq(seq_Ptr)=1;
            states_time(1,seq_Ptr)=t; % t_ini
            states_time(2,seq_Ptr)=t; % t_fin
            seq_Ptr = seq_Ptr + 1;
        end
        
    else
        %% CASE 2: ACTIVE NOTES AT FRAME T
        
        % Is important to keep sorted the tuple [note,channel,table_index]
        % to easy check if the state is still active or NOT (i.e. new 
        % score unit at t)
        % NOTE: MATLAB sortrows is aimed to sort the notes (descend order), 
        % then in case or same note channel is sorted in descend order too. 
        % Idem for the table index. 
        aux_sorted = sortrows([active_notes',active_channels',table_index']);
        active_notes = aux_sorted(:,1)';
        active_channels = aux_sorted(:,2)';
        table_index = aux_sorted(:,3)';
        
        % Check if the state is still active (same notes and channels actives)
        new_state = true;
        if note_counter == size(states_notes{states_seq(seq_Ptr-1)},2) % This is a fast control
            % IT IS CRUCIAL TO HAVE NOTES/CHANNELS SORTED HERE
            if all([active_notes;active_channels] == states_notes{states_seq(seq_Ptr-1)})
                %% CASE 2.1: THE PREVIOUS STATE CONTINUES 
                new_state = false;
                states_time(2,seq_Ptr-1) = t;
                % If new events in the table associated to the current state 
                % (i.e. consecutive repetitions), set the state info
                for ind=1:numel(table_index)
                    if table(table_index(ind),2) == 0
                        table(table_index(ind),2) = seq_Ptr-1;
                    end
                end
            end
        end
        
        %% CASE 2.2: NEW STATE AT FRAME T
        if new_state
            % Check if the score unit existed before or not.
            state_index = 0; s=1;
            while state_index==0 && s<=comb_Ptr-1
                if note_counter == size(states_notes{s},2)
                    if all([active_notes;active_channels] == states_notes{s})
                        state_index=s;
                    end
                end
                s=s+1;
            end
            
            %% CASE 2.2.1: NEW STATE: BUT THE SCORE UNIT APPEARED BEFORE
            if state_index~=0
                states_seq(seq_Ptr)=state_index;
                states_time(1,seq_Ptr)=t; % t_ini
                states_time(2,seq_Ptr)=t; % t_fin
                % Update table info
                for ind=1:numel(table_index)
                    if table(table_index(ind),2) == 0
                        table(table_index(ind),2) = seq_Ptr;
                    end
                end
                
                % UPDATE in the sequence index
                seq_Ptr=seq_Ptr+1;
            else
                %% CASE 2.2.2: NEW SCORE UNIT AND STATE
                states_notes{comb_Ptr}=[active_notes;active_channels]; 
                states_seq(seq_Ptr)=comb_Ptr; % State number
                states_time(1,seq_Ptr)=t; % t_ini
                states_time(2,seq_Ptr)=t; % t_fin
                % Update table info
                for ind=1:numel(table_index)
                    if table(table_index(ind),2) == 0
                        table(table_index(ind),2) = seq_Ptr;
                    end
                end   
                % UPDATE the score unit (combination) index
                comb_Ptr=comb_Ptr+1;
                % UPDATE the sequence index    
                seq_Ptr=seq_Ptr+1;
                
            end
        end
    end
    
    % ADD new block of memory if needed
    if( seq_Ptr+(BLOCK_SIZE/10) > seq_Size )  % less than 10%*BLOCK_SIZE free slots
        seq_Size = seq_Size + BLOCK_SIZE;     % add new BLOCK_SIZE slots
        states_notes{seq_Size} = [];
        states_seq(seq_Ptr+1:seq_Size) = 0;
        states_time(:,seq_Ptr+1:seq_Size) = 0;
    end
end

% NUMBER OF SCORE UNITS (<= NUMBER OF STATES (SEQUENCE))
k_max = comb_Ptr - 1; 

% REMOVE unused slots
states_notes = states_notes(1:seq_Ptr);
states_seq(seq_Ptr:end) = [];
states_time(:,seq_Ptr:end) = [];

return;