function [ids, vals, pths] = shizmidi(wavfile)
%SHIZMIDI   Match WAV file with score database.
%   [IDS, VALS, PTHS] = SHIZMIDI(WAV) matches WAV with all scores
%   in a database to identify the composition.
%
% input:
%   WAV : wav file
%
% output:
%   IDS  : cell of MIDI IDs sorted by similarity with the WAV 
%   VALS : matching values
%   PTHS : alignment paths
%

cpuaudio = '../remas/bin/CPUAudio ../remas/bin/Parameters.dat';
cpuremas = '../remas/bin/CPUReMAS';

% Score database
folder_dat = '../SCORE_DATA_SET/DAT/';

% List of scores
D = dir(folder_dat);
D = D(3:end);
numMidis = numel(D);

% Output buffers
ids  = cell (numMidis, 1);
vals = zeros(numMidis, 1);
pths = cell (numMidis, 1);

% Extract features from WAV
[~, wavname] = fileparts(wavfile);
wavfeatfile  = [wavname '.cfreq'];

system([cpuaudio ' ' wavfile ' ' wavfeatfile]);

% Matches audio with each MIDI in the database
tic
parfor i = 1:numMidis,
    sconame = D(i).name;
    datfile = [folder_dat sconame '/Parameters.dat'];
    outfile = [wavname '-vs-' sconame];

    % Calculate matching value using REMAS
    system([cpuremas ' ' datfile ' ' wavfeatfile ' ' outfile]);
    fid = fopen([outfile '.vmin']);

    % Store matching value and DTW path for this MIDI
    ids{i}  = sconame;
    vals(i) = fread(fid, 1, 'double');
    pths{i} = load([outfile '.path']);

    % Delete files
    fclose(fid);
    delete([outfile '.path']);
    delete([outfile '.vmin']);
end
toc
delete(wavfeatfile);

% Sort MIDIs according to matching
[vals, idx] = sort(vals);
ids  = ids(idx);
pths = pths(idx);

% Display result
fprintf('Identified score: %s (matching = %f)\n', ids{1}, vals(1));
