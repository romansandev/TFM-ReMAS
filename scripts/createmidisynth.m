% createmidisynth.m
%
% Converts MIDI files (*.mid) in a folder to synthesized WAV.
%
% 2018
%

% Input and output folders
folder_mid = '/home/pcabanas/discosoundnas/Shizmidi/SCORE_DATA_SET/MIDI/';
folder_syn = '/home/pcabanas/discosoundnas/Shizmidi/SCORE_DATA_SET/SYNTH/';

% Libraries adding
addpath ./mid2mtb;
addpath ./midi_lib;
javaaddpath('./midi_lib/KaraokeMidiJava.jar');

% Create output folder, read input folder
mkdir(folder_syn);
D = dir([folder_mid '*.mid']);

for i = 1:numel(D),
    name    = D(i).name(1:end-4);
    midfile = [folder_mid D(i).name];
    synfile = [folder_syn name '.wav'];
    tmpfile = [tempname('.') '.mid'];

    % Read MIDI
    nmat = mid2mtb(midfile);
    nmat(nmat(:,9)==0, 9) = 1;

    % Write temporary MIDI
    writemidi_seconds(nmat(:,1:8), tmpfile, nmat(:,[3 8 9]));

    % Convert to wav
    fluidsynth(tmpfile, synfile);

    delete(tmpfile);
end
