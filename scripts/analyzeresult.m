% analyzeresult.m
%
% Displays information of a results file (.mat).
%
% 2018
%

% Input file
matfile = '/home/pcabanas/discosoundnas/Shizmidi/results/20/id_1872_performance.mat';

% Score database
folder_mid = '/home/pcabanas/discosoundnas/Shizmidi/SCORE_DATA_SET/MIDI/';
folder_dat = '/home/pcabanas/discosoundnas/Shizmidi/SCORE_DATA_SET/DAT/';

% Queries folder
folder_qry = '/home/pcabanas/discosoundnas/Shizmidi/queries/20/';

% Load info (vals, ids, rank, pth1, ptht, x1, xt)
load(matfile);
pth1 = pth1 + 1;
ptht = ptht + 1;
clear x1 xt;

% Add library
addpath ./mid2mtb;
addpath ./NMF_States_Toolbox;

% Load audio query and extract features
[~, wavname] = fileparts(matfile);
wavfile = [folder_qry wavname '.wav'];

ry    = audioread(wavfile);
ry    = sum(ry, 2)';
ry    = ry/max(abs(ry));
rY_ft = computeCfreq(ry, fftParams(ry,44100), 1);
rY_ft = computeNorma_B(rY_ft, 1.5);

clear wavfile ry;

% Load top-1 score
sconame = ids{1};
scofile = [folder_dat sconame '/Parameters.dat'];
midfile = [folder_mid sconame '.mid'];

[~, sfk, states_time, states_seq] = readtrainremas(scofile);
ntramas = states_time(2,end);
nstates = size(states_time, 2);
nmat    = mid2mtb(midfile);
table   = getNMATdataChannel(nmat, []);
[~, ~, ~, seq_notes] = NMF_states_generateEchannel(struct('t_max',ntramas), table);
times_seq = zeros(1, ntramas);
for i = 1:nstates,
    times_seq(states_time(1,i):states_time(2,i)) = states_seq(i);
end


% Compare audio and aligned top-1 score
figure; hold on;
imagesc(rY_ft);
title('Audio vs Top-1 score (aligned)');
xlabel('Audio frame');
ylabel('Frequency (MIDI)');

s1Y_ft = zeros(size(rY_ft));

pth_seq = times_seq(pth1(:,2));
idx = [0 find(diff(pth_seq)) length(pth1)];
for i = 2:length(idx),
    k  = pth_seq(idx(i));
    u1 = pth1(idx(i-1)+1, 1);
    u2 = pth1(idx(i),     1);
    s1Y_ft(:,u1:u2) = repmat(sfk(:,k), 1, u2-u1+1);
    u1 = u1 - 0.5;
    u2 = u2 + 0.5;
    if k~=1,
        for note = seq_notes{k}(1,:),
            v1 = note - 23 - 0.5;
            v2 = note - 23 + 0.5;
            patch('Vertices', [u1 v1; u2 v1; u2 v2; u1 v2], 'Faces', [1 2 3 4], ...
                  'EdgeColor', 'w', 'FaceColor', 'none', 'LineWidth', 2);
        end
    end
end
set(gca, 'XLim', [0.5 size(rY_ft,2)+0.5], 'YLim', [0.5 114.5]);

figure;
imagesc(s1Y_ft);
set(gca, 'YDir', 'normal');
title('Top-1 score bases (aligned)');
xlabel('Audio frame');
ylabel('Frequency (MIDI)');


% Load true score
sconame = ids{rank};
scofile = [folder_dat sconame '/Parameters.dat'];
midfile = [folder_mid sconame '.mid'];

[~, sfk, states_time, states_seq] = readtrainremas(scofile);
ntramas = states_time(2,end);
nstates = size(states_time, 2);
nmat    = mid2mtb(midfile);
table   = getNMATdataChannel(nmat, []);
[~, ~, ~, seq_notes] = NMF_states_generateEchannel(struct('t_max',ntramas), table);
times_seq = zeros(1, ntramas);
for i = 1:nstates,
    times_seq(states_time(1,i):states_time(2,i)) = states_seq(i);
end

% Compare audio and aligned true score
figure; hold on;
imagesc(rY_ft);
title('Audio vs True score (aligned)');
xlabel('Audio frame');
ylabel('Frequency (MIDI)');

stY_ft = zeros(size(rY_ft));

pth_seq = times_seq(ptht(:,2));
idx = [0 find(diff(pth_seq)) length(ptht)];
for i = 2:length(idx),
    k  = pth_seq(idx(i));
    u1 = ptht(idx(i-1)+1, 1);
    u2 = ptht(idx(i),     1);
    stY_ft(:,u1:u2) = repmat(sfk(:,k), 1, u2-u1+1);
    u1 = u1 - 0.5;
    u2 = u2 + 0.5;
    if k~=1,
        for note = seq_notes{k}(1,:),
            v1 = note - 23 - 0.5;
            v2 = note - 23 + 0.5;
            patch('Vertices', [u1 v1; u2 v1; u2 v2; u1 v2], 'Faces', [1 2 3 4], ...
                  'EdgeColor', 'w', 'FaceColor', 'none', 'LineWidth', 2);
        end
    end
end
set(gca, 'XLim', [0.5 size(rY_ft,2)+0.5], 'YLim', [0.5 114.5]);

figure;
imagesc(stY_ft);
set(gca, 'YDir', 'normal');
title('True score bases (aligned)');
xlabel('Audio frame');
ylabel('Frequency (MIDI)');


% Compare local cost along path for both scores
d1 = zeros(1, size(rY_ft,2));
dt = d1;
for i = 1:size(rY_ft,2),
    d1(i) = (1/(1.5*0.5)) * (1 - sum(rY_ft(:,i).*(s1Y_ft(:,i).^0.5)).^1.5);
    dt(i) = (1/(1.5*0.5)) * (1 - sum(rY_ft(:,i).*(stY_ft(:,i).^0.5)).^1.5);
end

figure; hold on;
plot(dt);
plot(d1);
set(gca, 'XLim', [0.5 size(rY_ft,2)+0.5]);
title('Local cost along path: True (blue) vs Top-1 (red)');
xlabel('Audio frame');
ylabel('Local cost');


clear i k note u1 u2 v1 v2 idx ntramas nstates table nmat;
clear states_time states_seq sfk seq_notes times_seq pth_seq;
clear sconame scofile midfile;


% Load GT true score
folder_gtr = '/home/pcabanas/discosoundnas/MUSICNET_DATA_SET/GT/';
midfile = [folder_gtr wavname(1:8) 'gt_all.mid'];
txtfile = [folder_qry wavname '.txt'];

lims = load(txtfile);
lims = lims/44100;
nmat = mid2mtb(midfile);
nmat = nmat(nmat(:,6)>lims(1) & nmat(:,6)<lims(2), :);
nmat(:,6) = nmat(:,6) - lims(1);

% Compare audio and GT true score
figure; hold on;
imagesc(rY_ft);
title('Audio vs True score (GT)');
xlabel('Audio frame');
ylabel('Frequency (MIDI)');

for i = 1:size(nmat,1),
    u1 = max(ceil((44100*nmat(i,6) - 5700/2 + 570)/570),1) - 0.5;
    u2 = max(ceil((44100*(nmat(i,6)+nmat(i,7)) - 5700/2 + 570)/570),1) + 0.5;
    v1 = nmat(i,4) - 23 - 0.5;
    v2 = nmat(i,4) - 23 + 0.5;
    patch('Vertices', [u1 v1; u2 v1; u2 v2; u1 v2], 'Faces', [1 2 3 4], ...
          'EdgeColor', 'w', 'FaceColor', 'none', 'LineWidth', 2);
end
set(gca, 'XLim', [0.5 size(rY_ft,2)+0.5], 'YLim', [0.5 114.5]);

clear i u1 u2 v1 v2;
clear midfile txtfile nmat lims;